# Dre_AdminSearch

#### Features

	Globale admin Suche nach
	- articles
	- categories
	- cms pages
	- users
	- orders
	- vendors
	- manufacturers
	- modules

# Installation

```
// hinzufügen des Repositorys
composer config repositories.bodynova/dre_adminsearch git git@bitbucket.org:bodynova/dre_adminsearch.git
// installieren
composer require bender/dre_adminsearch
```

# Screenshot

![OXID Adminsuche](screenshot.png)
